<?php 
    error_reporting(test panurom );
  session_start();
?>
<!DOCTYPE html>
<html lang="en" class=" js no-touch" style="">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<meta name="robots" content="INDEX,FOLLOW"/>
<meta name="msvalidate.01" content="84D59CBD1FB854\8422E9619B026BBCD5" />
<meta name="google-site-verification" content="google29f1b4bc030d8a65"/>

<!-- Chrome, Firefox OS and Opera -->
<!-- <meta name="theme-color" content="#37a749">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
 -->

<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#37a749">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#37a749">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#37a749">


<meta property="twitter:description" content="UKs best selling Takeaway Software for an Indian, Chinese, Italian, Thai, Sushi, Bangladeshi and much more, Takeaways and Restaurants. We provide Online Ordering System with 0% Commision.">
<meta property="og:description" content="UKs best selling Takeaway Software for an Indian, Chinese, Italian, Thai, Sushi, Bangladeshi and much more, Takeaways and Restaurants. We provide Online Ordering System with 0% Commision.">
<meta property="twitter:title" content="Fusion POS - Delivery Software | Online Ordering System | Epos System">
<meta property="og:type" content="website">
<meta property="og:title" content="Fusion POS - Delivery Software | Online Ordering System | Epos System">
<meta property="og:url" content="http://fusionpos.co.uk/">
<meta property="twitter:card" content="summary">
<meta property="og:site_name" content="Create e-commerce Website & Apps with your brand." />

<meta name="keywords" content="Fusionpos Demo, Takeaway software, POS, Online food ordering system, Pizza Software, Chinese takeaway software.">

<?php include "descriptin.php" ?>
<?php include "title.php" ?>

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Source+Sans+Pro" rel="stylesheet">

<link rel="stylesheet" href="css/material.css">
<link rel="stylesheet" href="css/mobirise-icons.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/tether.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style1.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/style2.css">
<link rel="stylesheet" href="css/mbr-additional.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--<script src="js/sticky.js"></script>
<link href="css/YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">
<script src="js/YTPlayer.min.js"></script>--->

<!--------how to works mobile scroll--------->
<!--<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery.vticker.js"></script>-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-98488418-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Facebook Pixel Code -->

<!-- End Facebook Pixel Code -->

<link rel='stylesheet' id='avada-woocommerce-css'  href='css/woocommerce.css' type='text/css' media='all' />

<style type="text/css">
    #particles-js {
      position: absolute;
      width: 100%;
      height: 100%;
      //background: linear-gradient(to bottom, rgba(117, 114, 113, 0.8) 10%, rgba(40, 49, 77, 0.8) 30%, rgba(29, 35, 71, 0.8) 50%, rgba(19, 25, 28, 0.8) 80%, rgba(15, 14, 14, .8) 100%), url(https://38.media.tumblr.com/tumblr_m00c3czJkM1qbukryo1_500.gif);
      background-repeat: no-repeat;
      background-size: cover;
      background-position: 50% 50%;
}

.text-xs-left {
    text-align: center !important;
}
.text-xs-center {
    text-align: center !important;
    width: 600px;

}
.text-xs-center-1 {
    text-align: center !important;
    width: 600px;
    margin-left: 25%;
}
@media (max-width:460px) {
    .feedback-box {
    border: 1px solid #d4d4d4;
    padding: 10px;
    border-radius: 5px;
    margin: 22px;
    font-size: 14px !important;
}
.text-xs-center {
    text-align: center !important;
    width: 370px;

}
.text-xs-center-1 {
    text-align: center !important;
    width: 370px;
    margin-left: 1%;

}  
}

@media (max-width:320px) {
    .feedback-box {
    border: 1px solid #d4d4d4;
    padding: 10px;
    border-radius: 5px;
    margin: 22px;
    font-size: 12px !important;
}
.text-xs-center {
    text-align: center !important;
    width: 300px;

}
 
 .text-xs-center-1 {
    text-align: center !important;
    width: 300px;
    margin-left: 1%;
}

}

  </style>
  <script>snowStorm.excludeMobile = false;</script>

</head>

<body>

<div class="ajax-loading" style="display:none">
  <img alt="Takeaway Software" src="images/loading.gif" />
</div>



<div id="menu-0" custom-code="true">
  <section>
    <nav class="navbar navbar-dropdown bg-color transparent navbar-fixed-top " id="sub-page-header" style="background:#fff !important;">
      <div class="container">
        <div class="mbr-table">
          <div class="mbr-table-cell"> <a class="navbar-caption" href="index"><img src="images/fusionpos-logo.png" alt="Takeaway Software" /></a> </div>
          
        </div>
      </div>
    </nav>
  </section>
</div>

<section class="mbr-cards mbr-section mbr-section-nopadding" id="features1-e" style="background-color: rgb(244, 244, 244); padding-bottom:20px; padding-top:60px; ">
  <div class="mbr-cards-row row striped">
    <div class="col-xs-12 col-lg-12 " style="padding-top: 0px; padding-bottom: 0px;">
      <div class="container">
          <!-- <div class="col-xs-12 col-md-12 col-lg-12 text-xs-center"> 
            <h2 class="mbr-section-title display-2">Feedback <span>  </span> </h2>
          </div> -->
      </div>
    </div>
  </div>    
</section>

<section class="" id="pricing-table2-f" style="background-color: rgb(255, 255, 255); padding-bottom:10px; padding-top:30px;">
  <div class="container ">
    <div class="row">
      <div class="col-xs-offset-0 col-md-offset-3 text-xs-center "> 
        <div class="col-xs-12 col-md-8 col-sm-12 text-xs-center">

          <div class=""> 
              <h2 class="mbr-section-title display-2">How was your experience with FusionPOS Support?  <span>  </span> </h2> 
            </div>

           
        </div>
      </div>
    </div>
  </div>
</section>

<script>
//var emotion='Great';
   $('.checkbox').click(function(){
emotion = $("input[name='defaultExampleRadios']:checked").val();
//alert(emotion);
});
$(document).ready(function(){
    $('#feedbackForm').submit(function(e){
 e.preventDefault();
  var eid='';
    var cid='';
    var msg=$("#contacts4-2o-message").val();
     var ms=$('.great').text();
     var client_name=$('#takeawayname').val();
     emotion = $("input[name='defaultExampleRadios']:checked").val();
$.ajax({
   type:"POST",
    url:"feedbackapi.php",
    data:"cid="+cid+"&eid="+eid+"&emotion="+emotion+"&client_name="+client_name+"&msg="+msg,
   success:function(data){
       console.log(data);
       window.location.href="https://www.fusionpos.co.uk/thankyou";
         $('.input-sm').val("");
         },
      error:function(e){
        console.log(e);
        alert('error');
      }
  }); 
});
});


// $('.checkbox .btn').on('click', function() {
// var hiddenField = $(this).closest('.form-group').find('input[type=text]'), val = hiddenField.val();
// hiddenField.val(val === "1" ? "0" : "1");
// $(this).toggleClass('btn-success btn-default');
// $(this).find('span.fa').toggleClass('fa-check-square-o fa-square-o');
// });

$('.checkbox .btn').on('click', function() {
  $(this).removeClass('btn-success');
  $(this).find('span.fa').removeClass('fa fa-square-o');
  var hiddenField = $(this).closest('.form-group').find('input[type=text]'), val = hiddenField.val();
  hiddenField.val(val === "1" ? "0" : "1");
  $(this).toggleClass('btn-success btn-default');
  $(this).find('span.fa').toggleClass('fa-check-square-o fa-square-o');
});
</script>

<style>
  /*.custom-control-label:before {
      color: #fff;
      border-color: #007bff;
      background-color: #007bff;
  }
  .custom-control-label:before {
     border-radius: 50%;
  }
  .custom-control-label:after {
    position: absolute;
    top: .25rem;
    left: -1.5rem;
    display: block;
    width: 1rem;
    height: 1rem;
    content: "";
    background: no-repeat 50%/50% 50%;
  }*/
  .great { color: #47b861; }
  .okay { color: #3c3c3c; }
  .not-good { color: #d91a00; }
  .feedback-box { border:1px solid #d4d4d4; padding: 10px; border-radius: 5px; margin: 22px; font-size: 20px; }
  .feedback-box:hover { background: #ecfae2; }
  .fed-box {  }
  .feedback-rating { font-family: Rubik,sans-serif; font-size: 20px; }
  .fed-send { padding: 10px 20px !important; }
 @media (min-width:320px) and (max-width:950px){
    .feedback-box { margin: 10px 10px; }
    .display-2 { font-size: 22px; color: #36363d !important; line-height: 32px; margin:15px 0  }
  }
  /*@media (min-width:480px) { 
    .feedback-box { margin: 10px 18px; }
    .display-2 { font-size: 23px !important; color: #36363d !important; line-height: 36px; }
  }*/
</style>

<section class="" id="pricing-table2-f" style="background-color: rgb(255, 255, 255); padding-bottom:10px; padding-top:30px;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12 col-sm-12 text-xs-center text-xs-center-1 "> 
				 <form id="feedbackForm" method="POST" class="form-horizontal">

              <div class="form-group checkbox feedback-box col-md-3 col-xs-3">
                 <label class="" for=""><span class="great">Great</span></label> <br />
                <input type="radio" class="custom-control-input" value="Great" checked id="defaultUnchecked" name="defaultExampleRadios">
  <label class="custom-control-label" for="defaultUnchecked"></label>
              </div>
              <div class="form-group checkbox feedback-box col-md-3 col-xs-3">
                 <label class="" for=""><span class="okay">Okay</span></label>  <br />
                 <input type="radio" class="custom-control-input" value="Okay" id="defaultUnchecked" name="defaultExampleRadios">
  <label class="custom-control-label" for="defaultUnchecked"></label>
              </div>
              <div class="form-group checkbox feedback-box col-md-3 col-xs-3">
                 <label class="" for=""><span class="not-good">Not Good</span></label>  <br />
                <!-- <button type="button" class="btn btn-default"><span class="fa fa-square-o"></span></button> -->
                 <input type="radio" class="custom-control-input" value="Not Good" id="defaultUnchecked" name="defaultExampleRadios">
  <label class="custom-control-label" for="defaultUnchecked"></label>
              </div>

              <div class="clearfix"></div>
              <br />
              <p class="feedback-rating">Restaurant / Takeaway Name </p>
            <div class="form-group col-md-12 col-xs-12">
              <input type="text" class="form-control input-sm input-inverse fst-cps"  id="takeawayname" name="takeawayname" required>
            </div>
            <div class="clearfix"></div>
            
            <br />
            <p class="feedback-rating">Please Share Your Valuable Feedback & Help Us Improve! </p>
            <div class="form-group col-md-12 col-xs-12">
              <textarea class="form-control input-sm input-inverse fst-cps" value="" name="message" placeholder=" " data-form-field="Message" rows="3" id="contacts4-2o-message" required></textarea>
            </div>
            <div class="clearfix"></div>
            <div class="mbr-buttons mbr-buttons--right btn-inverse">
              <button type="submit" class="btn btn-sm btn-black app-free" >Submit</button>
            </div>
            </form>
			</div>
		</div>
	</div>
</section>

